FROM node:8.6

RUN mkdir /wedreg

COPY package.json yarn.lock /wedreg/

WORKDIR /wedreg

EXPOSE 8080

RUN yarn install

VOLUME /wedreg

CMD npm start
