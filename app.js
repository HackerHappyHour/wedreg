const Vue = require('vue')

module.exports = function App(req){
  return new Vue({
    data: {
      url: req.url
    },
    template: `<div>The visited URL is: {{ url }}</div>`
  })
}
