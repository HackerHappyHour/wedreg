const fs = require('fs')
const app = require('./app')
const server = require('express')()
const vueServerRenderer = require('vue-server-renderer')

const renderer = vueServerRenderer.createRenderer({
	template: fs.readFileSync('./index.template.html', 'utf-8')
})

server.get('*', (req, res) => {
  renderer.renderToString(app(req), (err, html) => {
		res.end(html)
  })
})

server.listen(8080)
