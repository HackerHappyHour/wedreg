# wedreg

> A Wedding Registry

## Build Setup

Make sure you have [installed docker-compose](https://docs.docker.com/compose/install/)

```bash
$ git clone git@gitlab.com:HackerHappyHour ~/code/wedreg
$ cd ~/code/wedreg
$ docker-compose up
```
